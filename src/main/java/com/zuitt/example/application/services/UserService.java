package com.zuitt.example.application.services;

import com.zuitt.example.application.models.User;

import java.util.Optional;

public interface UserService {
    void createUser(User user);

    Optional<User> findByUsername(String name);
}
