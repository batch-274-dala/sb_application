package com.zuitt.example.application.controllers;
import com.zuitt.example.application.exceptions.UserException;
import com.zuitt.example.application.models.User;
import com.zuitt.example.application.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

public class UserController {

    @Autowired
    UserService userService;
    @RequestMapping(value = "/users/register", method = RequestMethod.POST)
    public ResponseEntity<Object> register(@RequestBody Map<String, String> body) throws UserException {
        String username = body.get("username");
        if (!userService.findByUsername(username).isEmpty()) {
            throw new UserException("Username already exists");
        }else{
            String password = body.get("password");
            String encodedPassword = new BCryptPasswordEncoder().encode(password);

            User newUser = new User(username, encodedPassword);
            userService.createUser(newUser);

            return new ResponseEntity<>("User Registered successfully", HttpStatus.CREATED);
        }
    }
}
