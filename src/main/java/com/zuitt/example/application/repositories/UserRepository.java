package com.zuitt.example.application.repositories;
import com.zuitt.example.application.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Object> {

    User findByUsername(String username);
}
